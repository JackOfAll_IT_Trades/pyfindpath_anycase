
# PyFindPath_AnyCase:
#
# This module contains a utility function useful for porting scripts designed to work
# on *non*-case-sensitive file systems to run on case-sensitive file systems. [:-P]
#
# (An aside:  Ugh!  Who thinks case-sensitive file systems are a good idea anyway?!
#  Who on earth thinks its a good idea to let users create different folders called
#  "Documents" and "documents"?  Someone who's never actually worked with *people*?!?)
#
# This code is copyright (C) June, 2016  JackOfAll_IT_Trades
# (JackOfAll.IT.Trades@gmail.com), except the "splitall()" function as listed below.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the modified Berkeley license.
#


import os
import os.path as path


# The following function has been shamelessly stolen from:
# https://www.safaribooksonline.com/library/view/python-cookbook/0596001673/ch04s16.html
#
# (It was published there under a "modified Berkeley license, which is considered among
#  the most liberal of licenses" by Trent Mick.  The original license text was found at:
#  https://www.safaribooksonline.com/library/view/python-cookbook/0596001673/pr02s03.html)
#
def splitall(path) :
	allparts = []
	while 1:
		parts = os.path.split(path)
		if parts[0] == path:  # sentinel for absolute paths
			allparts.insert(0, parts[0])
			break
		elif parts[1] == path: # sentinel for relative paths
			allparts.insert(0, parts[1])
			break
		else:
			path = parts[0]
			allparts.insert(0, parts[1])
	return allparts
#
# END splitall(path) : ...


def FindPath_AnyCase(inputPath) :
	fullMatchFound  = True
	foundPathString = ""

	pathComponentsList = splitall(inputPath)

	for currentPathComponent in pathComponentsList:
		#
		# First, check to see if the current (non-case-adjusted) path exists:
		#
		currentPathString = path.join(foundPathString, currentPathComponent)
		if path.exists(currentPathString) :
			foundPathString = currentPathString
			continue
		#
		# END if path.exists(currentPathString) : ...

		# *sigh*  The path wasn't found.  We'll have to do a case-insensitive search :-P
		#
		currentPathLower  = currentPathComponent.lower()
		currentMatchFound = False

		# Start at the last directory we 'found' (or some other suitable placeholder)
		# and list all of its subdirectories:
		#
		currentStartingPath = foundPathString
		if len(foundPathString) == 0:
			if currentPathComponent.startswith(os.path.sep) or \
			   currentPathComponent.startswith(os.path.altsep) :  # Matches "/" as the first path component...
				currentStartingPath = os.path.sep
			elif len(currentPathComponent) >= 2 and (currentPathComponent[1] == ':') :  # Matches "C:\" as the first path component...
				foundPathString = currentPathComponent
				continue
			else:
				currentStartingPath = "."
			#
			# END if currentPathComponent.startswith(os.path.sep) or ... : ...
			#     elif  ... and (currentPathComponent[1] == ':') : ...
			#     else: ...

		#
		# END if len(foundPathString) == 0: ...

		currentSubDirList = os.listdir(currentStartingPath)

		# Iterate over the list of subdirectories...
		#
		for currentSubDir in currentSubDirList:
			#
			# ...and if we find a case-insensitive match, append the original
			#    currentSubDir to foundPathString and break:
			#
			currentSubDir_Lower    = currentSubDir.lower()
			if currentPathLower   == currentSubDir_Lower:
				foundPathString    = path.join(foundPathString, currentSubDir)
				currentMatchFound  = True
				break
			#
			# END if currentPathLower == currentSubDir_Lower: ...

		#
		# END for currentSubDir in currentSubDirList: ...

		# Check whether we were able to match the currentSubDir.
		# If not, set fullMatchFound to False and bail out:
		#
		if not currentMatchFound:
			fullMatchFound = False
			break
		#
		# END if not currentMatchFound: ...

	#
	# END for currentPathComponent in pathComponentsList: ...

	if not fullMatchFound or (len(foundPathString) < len(inputPath)) :
		return None
	#
	# END if not fullMatchFound or (len(foundPathString) < len(inputPath)) : ...

	inputPathLower = inputPath.lower()
	foundPathLower = foundPathString.lower()

	if inputPathLower != foundPathLower:
		return None
	#
	# END if inputPathLower != foundPathLower: ...

	return foundPathString
#
# END FindPath_AnyCase(inputPath) : ...


# Tests below:
#
# foundPath = FindPath_AnyCase("C:\\^dOcUmEnTs\\sOfTwArE dEvElOpMeNt")
# print( "Found path!  [%s]" % foundPath )

